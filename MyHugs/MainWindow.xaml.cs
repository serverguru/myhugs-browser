﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace MyHugs
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		string _collectionsBase;
		string _selectedType = "Painted";
		string _previewFolder;
		string _selectedCollection;

		public MainWindow()
		{
			InitializeComponent();
			var coll = GetInstallationFolder();
			_collectionsBase = coll + "\\PC HugWare\\Collections";
			_previewFolder = _collectionsBase + "\\Thumbs\\" + _selectedType;
			LoadCollections();
		}

		private string GetInstallationFolder()
		{
			var folder = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\pchugware", @"deluxe", @"C:\PC HugWare\pcHugBug Deluxe");
			return folder;
		}

		private void LoadCollections()
		{
			DirectoryInfo collectionsDir = new DirectoryInfo(_collectionsBase);
			var collections = collectionsDir.GetFiles("*.jpg");
			foreach (var file in collections)
			{
				ListBoxItem item = new ListBoxItem();
				item.Content = file.Name.Replace(".jpg", "");
				item.Selected += item_Selected;
				collectionsList.Items.Add(item);
			}
		}

		private void LoadPreview(string folder)
		{
			if (collectionBrowser == null)
				return;
			collectionBrowser.Items.Clear();
			DirectoryInfo previewDirectory = new DirectoryInfo(folder);
			var images = previewDirectory.GetFiles();
			foreach (var image in images)
			{
				ListBoxItem item = new ListBoxItem();
				Image newImage = new Image();
				BitmapImage im = new BitmapImage();
				im.BeginInit();
				im.UriSource = new Uri(image.FullName);
				im.EndInit();
				newImage.Source = im;
				item.Content = newImage;
				item.Selected += previewSelected;
				collectionBrowser.Items.Add(item);
			}
		}

		public BitmapSource ToBitmapSource( string path )
		{
			using (System.Drawing.Imaging.Metafile emf = new System.Drawing.Imaging.Metafile(path))
			using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(emf.Width, (emf.Height)))
			using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
			{
				g.Clear(System.Drawing.Color.White);
				g.DrawImage(emf, 0, 0, (float)emf.Width, (float)emf.Height);
				return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(bmp.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
			}
		}

		void previewSelected( object sender, RoutedEventArgs e )
		{
			var item = sender as ListBoxItem;
			if (item != null)
			{
				var image = item.Content as Image;
				if (image != null)
				{
					var bitmap = image.Source as BitmapImage;
					if (bitmap != null)
					{
						var actualImage = bitmap.UriSource.OriginalString.Replace("Thumbs", "Images");
						FileInfo previewFile = new FileInfo(actualImage);
						BitmapSource source = null;
						if (previewFile.Exists)
						{
							BitmapImage prev = new BitmapImage();
							prev.BeginInit();
							prev.UriSource = new Uri(actualImage);
							prev.EndInit();
							source = prev;
						}
						else
						{
							actualImage = actualImage.Replace(".jpg", ".wmf");
							previewFile = new FileInfo(actualImage);
							if (previewFile.Exists)
							{
								try
								{
									source = ToBitmapSource(actualImage);
								}
								catch { }
							}
						}
						if (source != null)
							previewImage.Source = source;
					}
				}
			}
		}

		void item_Selected( object sender, RoutedEventArgs e )
		{
			var item = sender as ListBoxItem;
			if (item != null)
			{
				_selectedCollection = item.Content.ToString();
				LoadPreview(_previewFolder + "\\" + _selectedCollection);
			}
		}

		private void Black_MouseLeftButtonUp( object sender, RoutedEventArgs e )
		{
			_selectedType = "Black";
			_previewFolder = _collectionsBase + "\\Thumbs\\" + _selectedType;
			LoadPreview(_previewFolder + "\\" + _selectedCollection);
		}

		private void Color_MouseLeftButtonUp_1( object sender, RoutedEventArgs e )
		{
			_selectedType = "Color";
			_previewFolder = _collectionsBase + "\\Thumbs\\" + _selectedType;
			LoadPreview(_previewFolder + "\\" + _selectedCollection);
		}

		private void Painted_MouseLeftButtonUp_1( object sender, RoutedEventArgs e )
		{
			_selectedType = "Painted";
			_previewFolder = _collectionsBase + "\\Thumbs\\" + _selectedType;
			LoadPreview(_previewFolder + "\\" + _selectedCollection);
		}

		private void copyButton_Click_1( object sender, RoutedEventArgs e )
		{
			
			var bit = previewImage.Source as BitmapSource;
			Clipboard.SetImage(bit);
		}

		private void Window_KeyUp_1( object sender, KeyEventArgs e )
		{
			if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && e.Key == Key.C)
			{
				var bit = previewImage.Source as BitmapSource;
				Clipboard.SetImage(bit);
				e.Handled = true;
			}
		}
	}
}
